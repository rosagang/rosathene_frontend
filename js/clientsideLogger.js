class ClientsideLogger {
    constructor() {
        this.logs = [];
    }

    log(str) {
        this.logs.push(str);
    }
}; 

module.exports = ClientsideLogger;