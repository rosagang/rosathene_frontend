import React, { useState } from "react";
import Photolist from "./Photolist";
import Camera from "./Camera";
import Button from "react-bootstrap/Button";
import Container from "react-bootstrap/Container";
import Jumbotron from "react-bootstrap/Jumbotron";
import Navbar from 'react-bootstrap/Navbar';
import { Switch, BrowserRouter as Router, Route, Redirect } from 'react-router-dom';

import Home from './views/Home';
import NoMatch from './views/NoMatch';
import Login from './views/Login';
import Lobby from './views/Lobby';
import About from './views/About';
import Feedback from './views/Feedback';
import ImageLoading from './ImageLoading';
import MapVisualizer from './visualiser/MapVisualizer'

const App = () => {
  return (
    <Router>
        <Switch>
          <Route exact path="/" component={Home} />
          <Route exact path="/login" component={Login} />
          <Route exact path="/lobby" component={Lobby} />
          <Route exact path="/load/:step" component={ImageLoading} />
          <Route exact path="/load"><Redirect to='/load/0' /></Route>
          <Route exact path="/about" component={About} />
          <Route exact path="/map" component={MapVisualizer} />
          <Route exact path="/feedback" component={Feedback} />
          <Route component={NoMatch} />
        </Switch>
    </Router>
  );
};

export default App;
