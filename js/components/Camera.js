import React, {useEffect} from 'react';
import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';
import Box from '@material-ui/core/Box';
import Card from '@material-ui/core/Card';
import CardMedia from '@material-ui/core/CardMedia';
import { makeStyles } from '@material-ui/core/styles';
import CameraIcon from '@material-ui/icons/Camera';
import CameraTwoToneIcon from '@material-ui/icons/CameraTwoTone';
import IconButton from '@material-ui/core/IconButton';
import CardContent from '@material-ui/core/CardContent';

const useStyles = makeStyles(() => ({
  card: {
    borderRadius: '1rem',
    boxShadow: 'none',
    position: 'relative',
    minWidth: 200,
    '&:after': {
      content: '""',
      display: 'block',
      position: 'absolute',
      width: '100%',
      height: '100%',
      bottom: 0,
      zIndex: 1,
    },
  },
  content: {
    position: 'absolute',
    zIndex: 2,
    bottom: 0,
    width: '100%',
  },
}));

let width, height;

export default function Camera(props) {
    let cameraView;
    const styles = useStyles();

    useEffect(() => {
        const constraints = {video : { facingMode : "environment"}, audio : false};
        cameraView = document.getElementById('camera-view');

        navigator.mediaDevices
            .getUserMedia(constraints)
            .then(stream => {
                cameraView.srcObject = stream;
                console.log(cameraView);
                props.setCameraWidthAndHeight(cameraView.videoWidth, cameraView.videoHeight);
            })
            .catch(err => {
                //TODO handle errors, give user poss to retry
                // using the camera
                alert(err);
            });
    })

    return (
        <Container>
            <Box>
            <Card className={styles.card}>
              <video id="camera-view" autoPlay playsInline style={{width: '70%', height: '70%', borderRadius: '1rem', minWidth: 200,
                      }}></video>
              <CardContent>
              <Box py={3} px={2} className={styles.content}>
                <IconButton onClick={() => props.pictureCallback(cameraView)} style={{backgroundColor: "white"}}>
                  <CameraTwoToneIcon fontSize="large" />
                </IconButton>
              </Box>
              </CardContent>
            </Card>

            </Box>

        </Container>
    )
}
