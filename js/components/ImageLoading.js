import React, { useState } from "react";
import Photolist from "./Photolist";
import Camera from "./Camera";
import { Link, Redirect, useParams, useRouteMatch } from "react-router-dom";
import Button from "@material-ui/core/Button";
import Container from "@material-ui/core/Container";
import Grid from "@material-ui/core/Grid";
import Box from "@material-ui/core/Box";
import { Spinner } from "react-bootstrap";
import Textfield from "@material-ui/core/TextField";
import AppBar from "@material-ui/core/AppBar";
import Typography from "@material-ui/core/Typography";
import IconButton from "@material-ui/core/IconButton";
import ExitToAppIcon from "@material-ui/icons/ExitToApp";
import HomeIcon from "@material-ui/icons/Home";
import { makeStyles } from "@material-ui/core/styles";
import checkJWT from "../jwt";

import ToolbarRosa from "./elements/ToolbarRosa";
import NextAndPrevButtons from "./elements/NextAndPrevButtons";
import DrawableMap from "./googlemaps/DrawawbleMap";
import TrashDataUploader from "./TrashDataUploader";
import ImageFromFileChooser from "./elements/ImageFromFileChooser.";
import ClientsideLogger from "../clientsideLogger";

const useStyles = makeStyles({
  root: {
    background: "linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)",
    border: 0,
    borderRadius: 3,
    boxShadow: "0 3px 5px 2px rgba(255, 105, 135, .3)",
    color: "white",
    height: 48,
    padding: "0 30px",
  },
});

let clientsideLogger = new ClientsideLogger();

export default function ImageLoading(props) {
  let [pictures, setPictures] = useState(new Array());
  let [photoWidth, setphotoWidth] = useState(0);
  let [photoHeight, setphotoHeight] = useState(0);
  let [uploadStep, setUploadStep] = useState(0);
  let [locationPolygons, setlocationPolygons] = useState([]);
  let [mapCenter, setmapCenter] = useState({ lat: 0, lng: 0, empty: true });
  let [loading, setloading] = useState(false);
  let [finishedUpload, setfinishedUpload] = useState(false);
  let [date, setDate] = useState(new Date());
  const [mapZoom, setMapZoom] = useState(15);
  const [, forceUpdate] = useState();
  const canProceedFunctions = [
    () => {
      if (pictures.length === 0) {
        return [false, "please take pictures to upload"];
      } else {
        return [true, ""];
      }
    },
    () => {
      console.log(locationPolygons);
      if (locationPolygons.length === 0) {
        return [
          false,
          "please draw a polygon indicating where you think the trash is located",
        ];
      } else {
        return [true, ""];
      }
    },
    (_) => [false, ""],
  ];

  let { step } = useParams();
  let stepNum = parseInt(step);
  console.log("stepnum ", stepNum);
  if (isNaN(stepNum) || stepNum >= 3) {
    stepNum = 0;
  }
  let i = 0;
  for (; i < stepNum; ++i) {
    let res = canProceedFunctions[i]();
    console.log(res);
    if (!res[0]) {
      console.log("cant proceed at step " + i);
      props.history.replace("/load/" + i);
      break;
    }
  }
  uploadStep = i;
  console.log("uploadstep: ", uploadStep);

  function pictureTaken(videoElement) {
    const canvas = document.getElementById("canvas");
    canvas.width = videoElement.videoWidth;
    canvas.height = videoElement.videoHeight;
    canvas.getContext("2d").drawImage(videoElement, 0, 0);
    pictures.push(canvas.toDataURL());
    forceUpdate({});
  }

  function removePhoto(i) {
    pictures.splice(i, 1);
    forceUpdate({});
    // function body
    // optional return;
  }

  function polyAdd(poly) {
    locationPolygons.push(poly);
    setlocationPolygons(locationPolygons);
    forceUpdate({});
  }

  function upload() {
    clientsideLogger.log("enter upload");
    if (loading) return;
    new Promise((toplevelRes) => {
      let formdata = new FormData();
      formdata.append("date", date.getTime());
      let promises = [];
      const canvas = document.getElementById("canvas");
      for (let i = 0; i < pictures.length; ++i) {
        let img = new Image();
        let promise = new Promise((resolve) => {
          img.onload = (_) => {
            clientsideLogger.log("img " + i + " loaded");
            clientsideLogger.log("img src: " + img.src.substring(0, 200));
            canvas.width = img.width;
            canvas.height = img.height;
            canvas.getContext("2d").drawImage(img, 0, 0);
            canvas.toBlob(resolve, "image/png");
          };
        }).then((blob) => {
          const file = new File([blob], "picture_" + i, { type: "image/png" });
          clientsideLogger.log(
            "file " + file.name + " created, size " + JSON.stringify(file.size)
          );
          formdata.append("picture", file);
        });
        promises.push(promise);
        img.src = pictures[i];
        clientsideLogger.log("img src set for " + i);
      }

      Promise.all(promises).then((_) => {
        let polys = [];
        for (let i = 0; i < locationPolygons.length; ++i) {
          const poly = locationPolygons[i];
          const path = poly.getPath();
          let pathArr = [];
          for (let j = 0; j < path.getLength(); ++j) {
            pathArr.push(path.getAt(j).toJSON());
          }
          polys.push(pathArr);
        }
        console.log(polys);
        formdata.append("polys", JSON.stringify(polys));

        formdata.append("logs", JSON.stringify(clientsideLogger.logs));
        for (var pair of formdata.entries()) {
          console.log(pair[0] + ", " + pair[1].toString());
          clientsideLogger.log(pair[0] + ", " + pair[1]);
        }
        Promise.race([
          fetch(process.env.STORAGE_BACKEND_URL, {
            method: "POST",
            body: formdata,
            headers: {
              jwt: window.localStorage.getItem("jwt"),
            },
          }),
          new Promise((_, rej) => {
            setTimeout((_) => rej(new Error("timeout")), 4000);
          }),
        ])
          .then(
            (response) => {
              if (response.status === 200) {
                setfinishedUpload(true);
              } else {
                console.log(response);
                setloading(false);
              }
            },
            (err) => {
              console.log(err);
              console.log("asdasdasdasdasdaasdasdasdasdasdasdasdasdasdasd");
              fetch("/errorlog", {
                method: "POST",
                body: JSON.stringify(clientsideLogger.logs),
                headers: {
                  "Content-Type": "application/json",
                  jwt: window.localStorage.getItem("jwt"),
                },
              }).then((res) => {
                setloading(false);
                console.log("res: ", res);
              });
              setloading(false);
            }
          )
          .then((_) => {
            toplevelRes();
          })
          .catch((err) => {
            console.log("asdasdasdasdasdaasdasdasdasdasdasdasdasdasdasd");
            console.log("errname %s", err.name);
            if (err.name === "timeout") {
              let errFormdata = new FormData();
              errFormdata.append("logs", JSON.stringify(clientsideLogger.logs));
              fetch("/errorlog", {
                method: "POST",
                body: errFormdata,
                headers: {
                  jwt: window.localStorage.getItem("jwt"),
                },
              }).then((res) => {
                setloading(false);
                console.log("res: ", res);
              });
            }
          });
      });
    });
    setloading(true);
  }

  function setCameraWidthAndHeight(width, height) {
    console.log(width + " ");
    setphotoWidth(width);
    setphotoHeight(height);
  }

  function removePolys() {
    for (let i = 0; i < locationPolygons.length; ++i) {
      locationPolygons[i].setMap(null);
    }
    setlocationPolygons([]);
  }

  function imageFromFile(files) {
    let promises = [];
    for (let i = 0; i < files.length; ++i) {
      let promise = new Promise((res) => {
        let fr = new FileReader();
        fr.onload = (_) => {
          pictures.push(fr.result);
          res();
        };
        fr.readAsDataURL(files[i]);
      });
      promises.push(promise);
    }
    Promise.all(promises).then((_) => {
      forceUpdate({});
    });
  }

  const classes = useStyles();

  const [valid, err] = checkJWT(window.localStorage.getItem("jwt"));
  if (!valid) {
    console.log(err);
    window.localStorage.removeItem("jwt");
    return <Redirect to="/"></Redirect>;
  }

  if (finishedUpload) return <Redirect to="/lobby" />;

  if (loading)
    return (
      <Grid
        container
        spacing={0}
        direction="column"
        alignItems="center"
        justify="center"
        style={{ minHeight: "100vh" }}
      >
        <Spinner animation="border" role="status">
          <span className="sr-only">Loading...</span>
        </Spinner>
      </Grid>
    );

  let pageContent;
  if (uploadStep === 0) {
    pageContent = (
      <Box align="center">
        <br />
        <Photolist
          pictures={pictures}
          removePhoto={removePhoto}
          photoWidth={photoWidth}
          photoHeight={photoHeight}
        ></Photolist>
        <br />
        <Camera
          pictureCallback={pictureTaken}
          setCameraWidthAndHeight={setCameraWidthAndHeight}
        ></Camera>
        <ImageFromFileChooser imagesChosen={imageFromFile} />
      </Box>
    );
  } else if (uploadStep === 1) {
    pageContent = (
      <Box align="center">
        <DrawableMap
          removePolys={removePolys}
          polyAdd={polyAdd}
          polygons={locationPolygons}
          center={mapCenter}
          centerChanged={(cen) => setmapCenter(cen)}
          zoom={mapZoom}
          zoomChanged={(zoom) => setMapZoom(zoom)}
        />
      </Box>
    );
  } else if (uploadStep === 2) {
    pageContent = (
      <Box align="center">
        <TrashDataUploader onDateChange={setDate} date={date} upload={upload} />
      </Box>
    );
  }

  function changeStep(step) {
    console.log("pushing state /load/" + step);
    props.history.push("/load/" + step);
    forceUpdate({});
  }

  return (
    <Grid
      container
      spacing={0}
      direction="column"
      alignItems="center"
      justify="center"
      style={{ minHeight: "100vh" }}
    >
      <ToolbarRosa />
      <canvas style={{ display: "none" }} id="canvas"></canvas>
      {pageContent}
      <NextAndPrevButtons
        onNext={(_) => {
          changeStep(uploadStep + 1);
        }}
        onPrev={(_) => {
          changeStep(uploadStep - 1);
        }}
        currentPage={uploadStep}
        amtPages={3}
        canProceedFunction={canProceedFunctions[uploadStep]}
      />
      <img id="test" style={{ visibility: "hidden" }}></img>
    </Grid>
  );
}
