import React, { useState } from "react";
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import GridListTileBar from '@material-ui/core/GridListTileBar';
import IconButton from '@material-ui/core/IconButton';
import HighlightOffIcon from '@material-ui/icons/HighlightOff';
import StarBorderIcon from '@material-ui/icons/StarBorder';
import { makeStyles } from '@material-ui/core/styles';


const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    width: '40%',
    maxWidth: 400,
    justifyContent: 'space-around',
    overflow: 'hide',
    backgroundColor: theme.palette.background.paper,
  },
  gridList: {
    flexWrap: 'nowrap',
    // Promote the list into his own layer on Chrome. This cost memory but helps keeping high FPS.
    transform: 'translateZ(0)',
    backgroundColor: "#E5E5E5",
  },
  title: {
    color: theme.palette.primary.light,
  },
  titleBar: {
    background:
      'linear-gradient(to top, rgba(0,0,0,0.7) 0%, rgba(0,0,0,0.3) 70%, rgba(0,0,0,0) 100%)',
  },
}));


export default function Photolist(props) {

    let items = [];

    function removePicture(i) {
      props.removePhoto(i);
    }

    for(let i = 0; i < props.pictures.length; ++i) {
        const currentURL = props.pictures[i];
        items.push(

          <GridListTile key={i}>
            <img src={currentURL} alt={i} key={i} />
            <GridListTileBar
              actionIcon={
                <IconButton aria-label={`star ${i}`} onClick={_ => removePicture(i)} style={{color: "#f2f2f2"}}>
                  <HighlightOffIcon />
                </IconButton>
              }
            />
          </GridListTile>

        )
    }
    const classes = useStyles();
    return (
        <div className={classes.root}>
        <GridList className={classes.gridList} cols={2.5}>
              {items}
          </GridList>
        </div>
    )
}
