import React from 'react'
import GridList from '@material-ui/core/GridList';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    width: 400,
    justifyContent: 'space-around',
    overflow: 'scroll',
    backgroundColor: theme.palette.background.paper,
  },
  gridList: {
    flexWrap: 'nowrap',
    // Promote the list into his own layer on Chrome. This cost memory but helps keeping high FPS.
    transform: 'translateZ(0)',
  },
  title: {
    color: theme.palette.primary.light,
  },
  titleBar: {
    background:
      'linear-gradient(to top, rgba(0,0,0,0.7) 0%, rgba(0,0,0,0.3) 70%, rgba(0,0,0,0) 100%)',
  },
}));


export default function Photolist(props) {

    let items = [];

    for(let i = 0; i < props.pictures.length; ++i) {
        const currentURL = props.pictures[i];
        items.push(<img width={props.photoWidth} height={props.photoHeight} className="col-sm"  src={currentURL} alt={i} key={i}></img>)
    }
    const classes = useStyles();
    return (
        <div className={classes.root}>
          <GridList className={classes.gridList} cols={2.5}>
            {items}
          </GridList>
        </div>
    )
}
