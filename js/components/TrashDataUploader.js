import React from "react";
import { Button, TextField } from "@material-ui/core";
import DateTimePicker from "react-datetime-picker";

export default function TrashDataUploader(props) {
  const canupload = props.date === null;
  return (
    <div>
      please enter when the photos were taken
      <DateTimePicker onChange={props.onDateChange} value={props.date} />
      <Button
        onClick={(_) => {
          props.onDateChange(new Date());
        }}
      >
        now
      </Button>
      <Button disabled={canupload} color="primary" onClick={props.upload}>
        upload
      </Button>
    </div>
  );
}
