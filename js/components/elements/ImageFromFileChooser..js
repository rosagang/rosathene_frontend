import React, {useEffect} from 'react'

export default function ImageFromFileChooser(props) {
    useEffect(() => {
        let input = document.getElementById('imagesFromDisk');
        input.onchange = _ => {
            props.imagesChosen(input.files);
        }
    })
    return (
        <div>
            <input multiple id='imagesFromDisk' type="file" accept="image/png, image/jpeg"></input>
        </div>
    )
}
