import React, { useState } from "react";
import Button from "@material-ui/core/Button";

export default function NextAndPrevButtons(props) {
    let [tooltipShown, settooltipShown] = useState(false);

    function toggleToolTip() {
        settooltipShown(!tooltipShown);
    }

  const [canProceed, error] = props.canProceedFunction();

  const nextButton = <Button disabled={!canProceed} onClick={props.onNext}>next</Button>;
  const prevButton = <Button onClick={props.onPrev}>prev</Button>;
  const tooltipButton = canProceed ? <div /> : <Button onClick={toggleToolTip}>?</Button>;

  

  if(canProceed) {
      tooltipShown = false;
  }
  let toolTip = <div />;

  if(tooltipShown) {
    toolTip = <div>{error}</div>
  }

  if (props.currentPage === 0) {
    return (
        <div>
            {nextButton}
            {tooltipButton}
            {toolTip}
        </div>
    );
  } else if (props.currentPage === props.amtPages - 1) {
    return (
        <div>
            {prevButton}
        </div>
    );
  } else {
    return (
        <div>
            {prevButton}
            {nextButton}
            {tooltipButton}
            {toolTip}
        </div>
    );
  }
}
