import React, { useState } from "react";
import { Link, Redirect } from 'react-router-dom';
import Button from '@material-ui/core/Button';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import HomeIcon from '@material-ui/icons/Home';



export default function ToolbarRosa() {
  const [, forceUpdate] = useState();

  function logout() {
    window.localStorage.removeItem('jwt');
    <Redirect to="/"></Redirect>
  }


  return (

      <AppBar>
        <Toolbar>
          <IconButton edge="start" color="inherit" aria-label="home" component={Link} to="/lobby">
            <HomeIcon />
          </IconButton>
          <Typography variant="h6">RosAthene</Typography>

          <IconButton edge="end" color="inherit" aria-label="logout" onClick={logout} component={Link} to="/">
            <ExitToAppIcon />
          </IconButton>
        </Toolbar>
      </AppBar>
  );
}
