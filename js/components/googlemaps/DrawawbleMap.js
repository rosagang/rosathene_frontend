import React, { useState, useEffect } from "react";
import { loadGoogleLib } from "./googlemaps-loader";
import Container from "@material-ui/core/Container"; 
import "./DrawableMap.css";
import { Button } from "@material-ui/core";
const API_KEY = process.env.GOOGLE_API_KEY;

export default function DrawawbleMap(props) {

  useEffect(() => {
    loadGoogleLib(API_KEY).then((google) => {
      let map = new google.maps.Map(document.getElementById("map"), {
        center: props.center,
        zoom: props.zoom,
        streetViewControl: false,
        mapTypeControl: false, });
      if (props.center.empty && navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function (position) {
          var pos = {
            lat: position.coords.latitude,
            lng: position.coords.longitude,
          };
          map.setCenter(pos);
        });
      }
      map.addListener("center_changed", function () {
        props.centerChanged(map.center);
      });

      map.addListener("zoom_changed", function () {
        props.zoomChanged(map.zoom);
      });

      let drawingManager = new google.maps.drawing.DrawingManager({
        drawingMode: google.maps.drawing.OverlayType.MARKER,
        drawingControl: true,
        drawingControlOptions: {
          position: google.maps.ControlPosition.TOP_CENTER,
          drawingModes: ["polygon"],
        },
        markerOptions: {
          icon:
            "https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png",
        },
      });
      drawingManager.setMap(map);
      google.maps.event.addListener(
        drawingManager,
        "polygoncomplete",
        (poly) => {
          props.polyAdd(poly);
        }
      );
      for(let i = 0; i < props.polygons.length; ++i) {
        props.polygons[i].setMap(map);
      }
    });
  }, []);

  function removePolys() {
      for(let i = 0; i < props.polygons.length; ++i) {
        props.polygons[i].setMap(map);
      }
    props.removePolys();
  }

  return (
    <Container>
      <div id="map"></div>
      <Button onClick={removePolys}>remove Polys</Button>
    </Container>
  );
}
