async function loadGoogleLib(apiKey) {
  return new Promise((resolve) => {
    if (window.hasGoogleMapsApi) resolve(window.google);
    else {
      var script = document.createElement("script");
      script.src = `https://maps.googleapis.com/maps/api/js?key=${apiKey}&libraries=drawing&callback=initMap`;
      script.defer = true;

      // Attach your callback function to the `window` object
      window.initMap = function () {
        window.hasGoogleMapsApi = true;
        resolve(window.google);
      };

      // Append the 'script' element to 'head'
      document.head.appendChild(script);
    }
  });
}

module.exports = { loadGoogleLib };
