import React from 'react';
import { Link } from 'react-router-dom';
import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';
import Grid from "@material-ui/core/Grid";
import Box from '@material-ui/core/Box';
import { makeStyles } from '@material-ui/core/styles';
import {Redirect} from 'react-router-dom';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import Avatar from '@material-ui/core/Avatar';



import ToolbarRosa from '../elements/ToolbarRosa';

const useStyles = makeStyles((theme) => ({
  icon: {
    marginRight: theme.spacing(2),
  },
  avatar: {
    width: '60%',
    height: '60%',
    margin: 'auto',
  },
  heroContent: {
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(8, 0, 6),
  },
  heroButtons: {
    marginTop: theme.spacing(4),
  },
  cardGrid: {
    paddingTop: theme.spacing(8),
    paddingBottom: theme.spacing(8),
  },
  card: {
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
  },
  cardMedia: {

    borderRadius: '40%',
  },
  cardContent: {
    flexGrow: 1,
  },
  footer: {
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(6),
  },
  root: {
    background: 'linear-gradient(to bottom, rgb(21, 48, 114) 0%, rgb(115, 81, 164) 100%)',
    height:"100%",
    width:"100vw",
    margin: "0",
  },
}));

const cards = [1, 2, 3, 4, 5, 6, 7, 8, 9];

const About = () => {

  const classes = useStyles();
  return (

    <React.Fragment>
      <ToolbarRosa/>

      <CssBaseline />
      <main>
        {/* Hero unit */}
        <div className={classes.heroContent}>
          <Container maxWidth="sm">
           <br />
            <Typography component="h2" variant="h3" align="center" color="textPrimary" gutterBottom>
            <Box fontWeight="fontWeightLight" fontFamily="serif" color="#2a2a2a" m={1}>
              RosAthene
            </Box>

            </Typography>
            <hr />
            <br />
            <Typography variant="h5" align="center" color="textSecondary" paragraph>
              Every year, millions of tons of plastic are thrown into the ocean destroying marine ecosystems and harming tourism and the health of coastal communities. <br /> <br />
              RosAthene attracts civilian-gathered data, combines it with satellite data and deploys state-of-the-art AI to monitor plastic pollution sources, routes, trends and accumulation zones. <br /> <br />With this, we arm local governments and organisations with unique opportunities to effectively create and monitor waterway waste policies, address illegal dumping and organise cleanups in the right place at the right time.
            </Typography>
          </Container>
        </div>
        <Container className={classes.cardGrid} maxWidth="md">
          {/* End hero unit */}
          <Typography component="h2" variant="h3" align="center" color="textPrimary" gutterBottom>
            The Team
          </Typography>
          <hr />
          <Grid container spacing={4}>
          {/*members */}
              <Grid item key="hen" xs={12} sm={6} md={4}>
                <Card className={classes.card}>
                  <CardContent className={classes.cardContent}>
                    <Avatar className={classes.avatar} src={"./assets/img/hendrik.jpg"} />
                    {"\n"}
                    <Typography gutterBottom variant="h5" component="h2">
                      Hendrik Serruys
                    </Typography>
                    <Typography>
                      MS in Computer Sciece Engineering at the KU Leuven with a major in Artificial Intelligence
                    </Typography>
                  </CardContent>
                </Card>
              </Grid>
              {/*Raul*/}
              <Grid item key="raul" xs={12} sm={6} md={4}>
                <Card className={classes.card}>
                  <CardContent className={classes.cardContent}>
                  <Avatar className={classes.avatar} src={"./assets/img/raul.jpg"} />
                    <Typography gutterBottom variant="h5" component="h2">
                      Raul Maldonado
                    </Typography>
                    <Typography>
                      MS in Artificial Intelligence at the KU Leuven with over 3 years of professional experience.
                    </Typography>
                  </CardContent>
                </Card>
              </Grid>
              {/*Holger*/}
              <Grid item key="holga" xs={12} sm={6} md={4}>
                <Card className={classes.card}>
                  <CardContent className={classes.cardContent}>
                  <Avatar className={classes.avatar} src={"./assets/img/holger.jpg"} />
                    <Typography gutterBottom variant="h5" component="h2">
                      Holger Klein
                    </Typography>
                    <Typography>
                      B.Sc. in Computer Science at the KIT with over 2 years of experience in web development.
                    </Typography>
                  </CardContent>
                </Card>
              </Grid>
          </Grid>
        </Container>
      </main>
      {/* Footer */}
      <footer className={classes.footer}>
        <Typography variant="h6" align="center" gutterBottom>
          RosAthene
        </Typography>
        <Typography variant="subtitle1" align="center" color="textSecondary" component="p">
          Fighting the good fight.
        </Typography>
      </footer>
      {/* End footer */}
    </React.Fragment>

  );
};

export default About;
