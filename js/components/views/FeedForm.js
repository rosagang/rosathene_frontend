import React from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import Checkbox from '@material-ui/core/Checkbox';

export default function FeedForm(props) {

  function onChange(e) {
    props.setfeedcontent({ ...props.feedcontent, [e.target.name]: e.target.value });
  }

  return (
    <React.Fragment>
      <br/>
      <Grid container spacing={3}>
        <Grid item xs={12} sm={6}>
          <TextField
            required
            id="name"
            name="name"
            label="name"
            fullWidth
            autoComplete="given-name"
            onChange={onChange}

          />
        </Grid>
        <Grid item xs={12} sm={6}>
          <TextField
            required
            id="orgName"
            name="orgName"
            label="Organization name"
            fullWidth
            autoComplete="organization-name"
            onChange={onChange}
          />
        </Grid>
        <Grid item xs={12} sm={6}>
          <TextField
            required
            id="city"
            name="city"
            label="City"
            fullWidth
            autoComplete="shipping address-level2"
            onChange={onChange}
          />
          </Grid>

        <Grid item xs={12} sm={6}>
          <TextField
            required
            id="country"
            name="country"
            label="Country"
            fullWidth
            autoComplete="shipping country"
            onChange={onChange}
          />
        </Grid>
        <Grid item xs={12} sm={6}>
          <TextField
            required
            id="email"
            name="email"
            label="Email"
            fullWidth
            autoComplete="email"
            onChange={onChange}
          />
      </Grid>
      <FormControl fullWidth >
      <TextField
          id="filled-multiline-static"
          label="Comments"
          name="comments"
          multiline
          rows={4}
          variant="outlined"
          onChange={onChange}
        />
        </FormControl>
        <Grid item xs={12}>
          <FormControlLabel
            control={<Checkbox color="secondary" name="subscribe" value="yes" />}
            label="Subscribe to our mailing list"
            onChange={onChange}
          />
        </Grid>
      </Grid>
    </React.Fragment>
  );
}
