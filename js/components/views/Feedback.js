import React, { useState, useEffect } from "react";
import { makeStyles } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Paper from '@material-ui/core/Paper';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import Button from '@material-ui/core/Button';
import Link from '@material-ui/core/Link';
import Typography from '@material-ui/core/Typography';
import FeedForm from './FeedForm';
import axios from "axios";
import Grid from "@material-ui/core/Grid";
import {  Spinner} from "react-bootstrap";
import { Redirect } from "react-router-dom";


import ToolbarRosa from '../elements/ToolbarRosa';

const useStyles = makeStyles((theme) => ({
  appBar: {
    position: 'relative',
  },
  layout: {
    width: 'auto',
    marginLeft: theme.spacing(2),
    marginRight: theme.spacing(2),
    [theme.breakpoints.up(600 + theme.spacing(2) * 2)]: {
      width: 600,
      marginLeft: 'auto',
      marginRight: 'auto',
    },
  },
  paper: {
    marginTop: theme.spacing(3),
    marginBottom: theme.spacing(3),
    padding: theme.spacing(2),
    [theme.breakpoints.up(600 + theme.spacing(3) * 2)]: {
      marginTop: theme.spacing(6),
      marginBottom: theme.spacing(6),
      padding: theme.spacing(3),
    },
  },
  stepper: {
    padding: theme.spacing(3, 0, 5),
  },
  buttons: {
    display: 'flex',
    justifyContent: 'flex-end',
  },
  button: {
    marginTop: theme.spacing(3),
    marginLeft: theme.spacing(1),
  },
}));


export default function Feedback() {
  const classes = useStyles();

  const [feedcontent, setfeedcontent] = useState({ name: "", orgName: "", city: "", country: "", email: "", comments: "", subscribe: "" });
  const [loading, setLoading] = useState(false);
  const [sentFeed, setsentFeed] = useState(false);
  const apiUrl = "feedback";

  function sendFeedback(e) {
    e.preventDefault();
    const data = { name: feedcontent.name, orgName: feedcontent.orgName, city: feedcontent.city, country: feedcontent.country, email: feedcontent.email, comments: feedcontent.comments, subscribe: feedcontent.subscribe };
    axios.post(apiUrl, data).then((result) => {
      if (result.status == "200") {
        console.log(result.data);
        setsentFeed(true);
      }
      else {
        console.log("Errrror");
      }
    }).then(_ => {
      setLoading(false);
    });
    setLoading(true);
  }
  if(loading) return (
    <Grid
      container
      spacing={0}
      direction="column"
      alignItems="center"
      justify="center"
      style={{ minHeight: '100vh' }}
    >
      <Spinner animation="border" role="status">
        <span className="sr-only">Loading...</span>
      </Spinner>
    </Grid>
  );

  if(sentFeed) return (
    <Redirect to="/lobby"></Redirect>
  );

  return (
    <React.Fragment>
      <CssBaseline />

      <ToolbarRosa/>
      <br/>
      <br/>
      <main className={classes.layout}>
        <Paper className={classes.paper}>
          <Typography component="h1" variant="h4" align="center">
            Give Us Feedback!
          </Typography>
          <React.Fragment>
              <form onSubmit={sendFeedback}>
                <FeedForm feedcontent={feedcontent} setfeedcontent={setfeedcontent}/>
                <div className={classes.buttons}>
                  <Button
                    type="submit"
                    variant="contained"
                    color="primary"
                    className={classes.button}
                  >
                    Submit
                  </Button>
                </div>
            </form>
          </React.Fragment>
        </Paper>
      </main>
    </React.Fragment>
  );
}
