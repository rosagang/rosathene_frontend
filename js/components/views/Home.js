import React from 'react';
import { Link } from 'react-router-dom';
import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';
import Grid from "@material-ui/core/Grid";
import Box from '@material-ui/core/Box';
import { makeStyles } from '@material-ui/core/styles';
import {Redirect} from 'react-router-dom';

const useStyles = makeStyles({
  root: {
    background: 'linear-gradient(to bottom, rgb(21, 48, 114) 0%, rgb(115, 81, 164) 100%)',
    height:"100%",
    width:"100vw",
    margin: "0",
  },
  title: {
    dropShadow: "5px 5px 5px #666666",
    width:"50%",
    height:"50%",
  }
});

const Home = () => {
  if(window.localStorage.getItem("jwt")) {
    return (<Redirect to="/lobby"></Redirect>);
  }
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <Grid
        container
        spacing={0}
        direction="column"
        alignItems="center"
        justify="center"
        style={{ minHeight: '100vh' }}
      >
        <Box
          color="#ffffff"
          fontFamily="h6.fontFamily"
          align="center"
          fontSize={{ xs: 'h6.fontSize', sm: 'h4.fontSize', md: 'h3.fontSize' }}
          p={{ xs: 2, sm: 3, md: 4 }}
        >

         <img src={"./assets/img/aquire.png"} alt="RosAthene"  className={classes.title} />
        </Box>
        <Box align="center">

          <Button variant="contained" color="primary" component={Link} to="/login">
            Login
          </Button>
          <Button variant="contained" color="secondary" style={{margin: "10px", backgroundColor:"#8B008B"}} component={Link} to="/about">
            About us
          </Button>
          {"\n"}
        </Box>
        </Grid>
      </div>

  );
};

export default Home;
