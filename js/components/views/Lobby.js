import React from 'react';
import { Link } from 'react-router-dom';
import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';
import Grid from "@material-ui/core/Grid";
import Box from '@material-ui/core/Box';
import { makeStyles } from '@material-ui/core/styles';
import {Redirect} from 'react-router-dom';
import checkJWT from '../../jwt'


const useStyles = makeStyles({
  root: {
    background: 'linear-gradient(to bottom, rgb(21, 48, 114) 0%, rgb(115, 81, 164) 100%)',
    height:"100%",
    width:"100vw",
    margin: "0",
  },
});

const Lobby = () => {
  const [valid, err] = checkJWT(window.localStorage.getItem('jwt'));
  if(!valid) {
    console.log(err);
    window.localStorage.removeItem('jwt')
    return <Redirect to="/"></Redirect>
  }

  const classes = useStyles();
  return (
    <div className={classes.root}>
      <Grid
        container
        spacing={0}
        direction="column"
        alignItems="center"
        justify="center"
        style={{ minHeight: '100vh' }}
      >
        <Box align="center">

          <Button variant="contained" color="primary" component={Link} to="/load">
            Load Images
          </Button>
          <br />
          <br />
          <Button variant="contained" color="primary" component={Link} to="/about">
            About Us
          </Button>
          <br />
          <br />
          <Button variant="contained" color="primary" component={Link} to="/feedback">
            Give Us Feedback
          </Button>


          {"\n"}
        </Box>
        </Grid>
      </div>

  );
};

export default Lobby;
