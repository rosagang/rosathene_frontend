import React, { useState, useEffect } from "react";
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import TextField from "@material-ui/core/TextField";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import Link from "@material-ui/core/Link";
import Grid from "@material-ui/core/Grid";
import Box from "@material-ui/core/Box";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import axios from "axios";
import Alert from '@material-ui/lab/Alert';
import {  Spinner} from "react-bootstrap"
import { Redirect } from "react-router-dom";

function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {"Copyright © "}
      <Link color="inherit" href="/">
        Your Website
      </Link>{" "}
      {new Date().getFullYear()}
      {"."}
    </Typography>
  );
}

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

export default function Login() {
  const classes = useStyles();

  const [user, setuser] = useState({ username: "", password: "" });
  const [errors, setErrors] = useState();
  const [displayedErrors, setDisplayedErrors] = useState([]);
  const apiUrl = "login";
  const [loggedin, setLoggedIn] = useState(window.localStorage.getItem("jwt") ? true : false);
  const [loading, setLoading] = useState(false);

  function findErrors() {
    let found = [];
    if (user.username === "") {
      found.push("please enter a valid username");
    }
    if (user.password === "") {
      found.push("please enter a valid password");
    }
    return found;
  }

  function login(e) {
    e.preventDefault();
    let foundErrors = findErrors();
    setErrors(foundErrors);
    setDisplayedErrors(foundErrors);
    if(foundErrors.length !== 0) return;

    const data = { username: user.username, password: user.password };
    setLoading(true);
    axios.post(apiUrl, data).then((result) => {
      setLoading(false);
      if (result.status == "200") {
        console.log(result.data);
        const jwt = result.data.jwt;
        window.localStorage.setItem("jwt", jwt);
        setLoggedIn(true);
      }
      else {
        setuser({username : "", password : ""});
        setDisplayedErrors(["The Username or Password are incorrect"]);
      }
    });
  }

  function removeAtIdx(arr, idx) {
    let newArr = [];
    for(let i = 0; i < arr.length; ++i) {
      if(i !== idx) {
        newArr.push(arr[i]);
      }
    }
    return newArr;
  }

  function dismissError(i) {
    let newDisplayErrors = removeAtIdx(displayedErrors, i);
    setDisplayedErrors(newDisplayErrors);
  }

  function onChange(e) {
    setuser({ ...user, [e.target.name]: e.target.value });
    setErrors(findErrors());
  }

  if(loggedin) return (
    <Redirect to="/lobby"></Redirect>
  );

  if(loading) return (
    <Grid
      container
      spacing={0}
      direction="column"
      alignItems="center"
      justify="center"
      style={{ minHeight: '100vh' }}
    >
      <Spinner animation="border" role="status">
        <span className="sr-only">Loading...</span>
      </Spinner>
    </Grid>
  );

  let errorPopups = [];
  for (let i = 0; i < displayedErrors.length; ++i) {
    let popup = (
      <Alert key={i} severity="error" onClose={_ => dismissError(i)}>
      Invalid input
        <p>
          {displayedErrors[i]}
        </p>
      </Alert>

    );
    errorPopups.push(popup);
  }
  return (
    <Container component="main" maxWidth="xs">
    {errorPopups}
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Sign in
        </Typography>
        <form className={classes.form} noValidate onSubmit={login}>
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="username"
            label="Username"
            name="username"
            autoFocus
            onChange={onChange}
            value={user.username}
          />
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="password"
            label="Password"
            type="password"
            id="password"
            autoComplete="current-password"
            onChange={onChange}
            value={user.password}
          />
          <FormControlLabel
            control={<Checkbox value="remember" color="primary" />}
            label="Remember me"
          />
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
          >
            Sign In
          </Button>
          <Grid container>
            <Grid item xs>
              <Link href="#" variant="body2">
                Forgot password?
              </Link>
            </Grid>
            <Grid item>
              <Link href="#" variant="body2">
                {"Don't have an account? Sign Up"}
              </Link>
            </Grid>
          </Grid>
        </form>
      </div>
      <Box mt={8}>
        <Copyright />
      </Box>
    </Container>
  );
}
