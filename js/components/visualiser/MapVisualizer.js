import React, { useEffect } from "react";
import Container from "@material-ui/core/Container";
import { loadGoogleLib } from "../googlemaps/googlemaps-loader";
import axios from "axios";

const API_KEY = process.env.GOOGLE_API_KEY;
const POLY_URL = process.env.POLY_BACKEND_URL;

export default function MapVisualizer(props) {
  useEffect(() => {
    loadGoogleLib(API_KEY).then((google) => {
      let map = new google.maps.Map(document.getElementById("vismap"), {
        streetViewControl: false,
        mapTypeControl: false,
        zoom: 1,
        center: { lat: 0, lng: 0 },
      });
      axios.get(POLY_URL).then((result) => {
        const polys = result.data.polys;
        console.log(polys);
        for (let i = 0; i < polys.length; ++i) {
          const currentPolys = polys[i];
          console.log("currentpolys: ", currentPolys);
          for (let j = 0; j < currentPolys.coordinates.length; ++j) {
            const poly = currentPolys.coordinates[j];
            const path = poly;
            console.log("path: ", path);
            let coords = [];
            for(let k = 0; k < path.length; ++k) {
              coords.push({lat : parseFloat(path[k].lat), lng : parseFloat(path[k].lng)})
            }
            console.log('coords: ', coords);
            const shape = new google.maps.Polygon({
              paths: coords
            });
            shape.setMap(map);
          }
        }
      });
    });
  }, []);
  return (
    <div>
      <Container>
        <div id="vismap" style={{ width: "500px", height: "500px" }}></div>
      </Container>
    </div>
  );
}
