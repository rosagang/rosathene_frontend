const path = require("path");
const webpack = require("webpack");

module.exports = env => {
  const envKeys = Object.keys(env).reduce((prev, next) => {
        prev[`process.env.${next}`] = JSON.stringify(env[next]);
        return prev;
      }, {});

   return {
      entry: "./js/index.js",
      module: {
        rules: [
          {
            test: /\.(js|jsx)$/,
            exclude: /(node_modules|bower_components)/,
            loader: "babel-loader",
            options: { presets: ["@babel/env"] }
          },
          {
            test: /\.css$/,
            use: ["style-loader", "css-loader"]
          },
          {test: /\.(jpe?g|png|gif|svg)$/i, loader: "file-loader?name=/public/icons/[name].[ext]"}
        ]
      },
      resolve: { extensions: ["*", ".js", ".jsx"] },
      output: {
        path: path.resolve(__dirname, "../rosathene_frontend_be/public"),
        filename: "bundle.js"
      },
      plugins: [new webpack.DefinePlugin(envKeys)]
  };
};
